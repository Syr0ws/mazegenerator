import fr.syrows.maze.Maze
import fr.syrows.maze.generators.{MazeGenerator, RandomPathFusionMazeGenerator}
import org.junit.Test

import scala.util.Random

class TestRandomPathFusionMazeGenerator {

  private val RANDOM : Random = new Random

  @Test
  def testGenerateMaze(): Unit = {

    val generator : MazeGenerator = new RandomPathFusionMazeGenerator

    // Generating 100 mazes and testing them.
    for(_ <- 0 to 100) {

      var width = 3 + RANDOM.nextInt(100)
      var height = 3 + RANDOM.nextInt(100)

      if(width% 2 == 0) width += 1
      if(height % 2 == 0) height += 1

      val maze : Maze = generator.generateMaze(width, height)
      val grid : Array[Array[Int]] = maze.grid

      for(row <- grid.indices) {

        for(column <- grid(0).indices) {

          // Checking cells.
          if(row % 2 == 1 && column % 2 == 1) {

            assert(grid(row)(column) != -1) // Checking that the cell isn't a wall.
            assert(grid(row)(column) == grid(1)(1)) // Checking that the cell is linked to the others.
          }

          // Checking that intersections are wall.
          if(row % 2 == 0 && column % 2 == 0) assert(grid(row)(column) == -1)
        }
      }
    }
  }

}
