package fr.syrows.maze

object MazeUtils {

  /**
   * Iterates over the rows and columns of a grid that represents a maze and executes
   * a function for each couple (row,column).
   *
   * @param grid A 2x2 matrix that represents the grid of a maze.
   * @param function A function to execute on each couple (row, column).
   * @param step A number which represents the step of each loop.
   */
  def iterate(grid : Array[Array[Int]], function: (Int, Int) => Unit, step : Int = 1) : Unit = {

    for(row <- grid.indices by step) {

      for (column <- grid(0).indices by step) {

        function(row, column)
      }
    }
  }
}
