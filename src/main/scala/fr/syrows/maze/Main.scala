package fr.syrows.maze

import fr.syrows.maze.generators.{MazeGenerator, RandomPathFusionMazeGenerator}

object Main {

  def main(args: Array[String]): Unit = {

    genGrid(9, 23)
  }

  /**
   * Generate a maze.
   *
   * @param width the width of the maze.
   * @param height the height of the maze.
   */
  def genGrid(width : Int, height : Int) : Unit = {

    val generator : MazeGenerator = new RandomPathFusionMazeGenerator()
    val maze : Maze = generator.generateMaze(width, height)

    displayMaze(maze)
  }

  /**
   * Dislay a maze. The space represent empty space, the symbol '#' a wall
   * and the '.' the entrance and the exit.
   *
   * @param maze an object of type Maze to display.
   */
  def displayMaze(maze : Maze) : Unit = {

    val grid : Array[Array[Int]] = maze.grid

    val entry : MazeCell = maze.entry
    val exit : MazeCell = maze.exit

    MazeUtils.iterate(grid, (row : Int, column : Int) => {

      val value : Int = grid(row)(column)

      val symbol : String = {

        if(entry.row == row && entry.column == column || exit.row == row && exit.column == column) "."
        else if(value == MazeCell.MAZE_WALL_VALUE) "#"
        else " "
      }

      print(if(column + 1 == grid(row).length) symbol + "\n" else symbol)
    })
  }
}
