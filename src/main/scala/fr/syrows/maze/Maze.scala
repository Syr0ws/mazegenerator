package fr.syrows.maze

class Maze(val entry : MazeCell, val exit : MazeCell, val grid : Array[Array[Int]]) {

  require(entry != null, "Entry cannot be null.")
  require(exit != null, "Exit cannot be null.")

  require(grid != null, "Grid cannot be null.")
  require(grid.nonEmpty, "Grid cannot be empty.")

  def rows : Int = this.grid.length

  def columns : Int = this.grid(0).length

  def size : Int = this.rows * this.columns
}
