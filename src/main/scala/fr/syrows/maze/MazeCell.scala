package fr.syrows.maze

class MazeCell(val row : Int, val column : Int) {

  require(row >= 0, "Row cannot be negative.")
  require(column >= 0, "Column cannot be negative")
}

object MazeCell {

  def MAZE_WALL_VALUE : Int = -1
}
