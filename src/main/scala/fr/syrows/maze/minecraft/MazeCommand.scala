package fr.syrows.maze.minecraft

import fr.syrows.maze.generators.{MazeGenerator, RandomPathFusionMazeGenerator}
import org.bukkit.command.{Command, CommandExecutor, CommandSender}
import org.bukkit.entity.Player
import org.bukkit.plugin.Plugin
import org.bukkit.{Bukkit, Location, Material}

class MazeCommand(val plugin : Plugin) extends CommandExecutor {

  override def onCommand(sender : CommandSender, command : Command, label : String, args: Array[String]): Boolean = {

    if(!sender.isInstanceOf[Player]) return false

    val player : Player = sender.asInstanceOf[Player]
    val location : Location = player.getLocation()

    if(args.length != 3) {
      player.sendMessage("Invalid command. Must be /maze <rows> <columns> <height>.")
      return true
    }

    // Checking that all arguments are numbers.
    if(!this.isInt(args(0)) || !this.isInt(args(1)) || !this.isInt(args(2))) {
      player.sendMessage("Invalid arg type.")
      return true
    }

    val rows = args(0).toInt
    val columns = args(1).toInt
    val y = args(2).toInt

    // Checking that number of rows and columns is odd.
    if(rows % 2 == 0 || columns % 2 == 0) {
      player.sendMessage("Number of rows and columns must be odd.")
      return true
    }

    val generator : MazeGenerator = new RandomPathFusionMazeGenerator
    val maze = generator.generateMaze(rows, columns)
    val grid : Array[Array[Int]] = maze.grid

    // Generating blocks.
    val runnable : Runnable = () => {

      for(x <- grid.indices) {

        for(z <- grid(0).indices) {

          for(y <- 0 to y) {

            val posX = x + location.getBlockX
            val posY = y + location.getBlockY
            val posZ = z + location.getBlockZ

            var material : Material = Material.AIR

            // For floor and walls.
            if(y == 0 || grid(x)(z) == -1) material = Material.STONE

            location.getWorld.getBlockAt(posX, posY, posZ).setType(material)
          }
        }
      }
    }
    Bukkit.getScheduler.runTask(this.plugin, runnable)

    // Teleporting player to the entry of the maze.
    val entry : Location = new Location(
      location.getWorld,
      location.getBlockX + maze.entry.row,
      location.getBlockY,
      location.getBlockZ + maze.entry.column
    )

    player.teleport(entry)
    true
  }

  private def isInt(str : String) : Boolean = str.forall(c => c.isDigit)
}
