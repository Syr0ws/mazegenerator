package fr.syrows.maze.minecraft

import org.bukkit.plugin.java.JavaPlugin

class MazePlugin extends JavaPlugin {

  override def onEnable() {
    super.getCommand("maze").setExecutor(new MazeCommand(this))
  }
}
