package fr.syrows.maze.generators

import fr.syrows.maze.Maze

trait MazeGenerator {

  /**
   * Generate a maze with the specified dimensions.
   *
   * @param rows The number of rows of the maze.
   * @param columns The number of columns of the maze.
   * @return a Maze object.
   */
  def generateMaze(rows: Int, columns: Int): Maze
}
