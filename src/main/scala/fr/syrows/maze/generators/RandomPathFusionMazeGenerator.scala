package fr.syrows.maze.generators

import fr.syrows.maze.{Main, Maze, MazeCell, MazeUtils}

import scala.util.Random

class RandomPathFusionMazeGenerator extends MazeGenerator {

  private val RANDOM : Random = new Random()

  /**
   * Generate a maze using the random path fusion generator.
   *
   * Algorithm:
   * 1) Setting a unique value for each cell.
   * 2) Find a random intern wall (not the external ones).
   * 3) Retrieve the two cells linked by the wall. If they have the same value, return to step 1.
   * 4) Choose the value of one of the two cells. Replace the one of the other and of the wall by it.
   * 5) Replace all the old values (values of the second cell) by the new one.
   *
   * @param rows The number of rows of the maze.
   * @param columns The number of columns of the maze.
   * @return a Maze object.
   */
  override def generateMaze(rows: Int, columns: Int): Maze = {

    require(rows % 2 != 0, "Number of rows must be odd.")
    require(columns % 2 != 0, "Number of rows must be odd.")

    val grid : Array[Array[Int]] = this.generateGrid(rows, columns) // Generate maze's grid.
    val entries = this.generateEntries(rows, columns) // Generate entry and exit coordinates.

    // step 1
    var i = 0

    for(row <- 1 until rows by 2) {

      for (column <- 1 until columns by 2) {

        grid(row)(column) = i
        i += 1
      }
    }

    while(!this.isGenerated(grid)) {

      val wall : (Int, Int) = this.getRandomWall(grid) // step 2

      var cell1 : (Int, Int) = null
      var cell2 : (Int, Int) = null

      if(grid(wall._1 - 1)(wall._2) == -1) {

        // Horizontal
        cell1 = (wall._1, wall._2 - 1)
        cell2 = (wall._1, wall._2 + 1)

      } else {

        // Vertical
        cell1 = (wall._1 - 1, wall._2)
        cell2 = (wall._1 + 1, wall._2)
      }

      val cell1Value = grid(cell1._1)(cell1._2)
      val cell2Value = grid(cell2._1)(cell2._2)

      // Checking if cells are different (step 3).
      if(cell1Value != cell2Value) {

        grid(wall._1)(wall._2) = cell2Value // step 4
        this.replaceCells(grid, cell1Value, cell2Value) // step 5
      }
    }

    grid(entries._1.row)(entries._1.column) = 0 // Setting entrance.
    grid(entries._2.row)(entries._2.column) = 0 // Setting exit.

    new Maze(entries._1, entries._2, grid)
  }

  /**
   * Generate a maze grid.
   *
   * @param rows The number of rows of the grid.
   * @param columns The number of columns of the grid.
   * @return a two dimensional array in which all cell is surrounded by a wall.
   */
  def generateGrid(rows : Int, columns : Int) : Array[Array[Int]] = {

    require(rows % 2 != 0, "Number of rows must be odd.")
    require(columns % 2 != 0, "Number of rows must be odd.")

    // Generating a two dimensional array that represents the grid of the maze.
    val grid : Array[Array[Int]] = Array.ofDim[Int](rows, columns)

    MazeUtils.iterate(grid, (row : Int, column : Int) => {

      if(row % 2 == 0 || column % 2 == 0) grid(row)(column) = MazeCell.MAZE_WALL_VALUE
    })
    grid
  }

  /**
   * Get a random intern wall of the grid.
   *
   * @param grid the grid of the maze.
   * @return a couple of two coordinates that correspond to a wall in the maze.
   */
  private def getRandomWall(grid : Array[Array[Int]]) : (Int, Int) = {

    var wall: (Int, Int) = null

    while (wall == null) {

      val row = RANDOM.between(1, grid.length - 1)
      val column = RANDOM.between(1, grid(0).length - 1)

      // Checking that the cell is a wall and that it is not an intersection.
      if ((row % 2 != 0 || column % 2 != 0) && grid(row)(column) == -1) wall = (row, column)
    }
    wall
  }

  /**
   * Replace all the cells which have a specified value by another one.
   *
   * @param grid the grid of the maze.
   * @param from The value to replace.
   * @param to The value to replace with.
   */
  private def replaceCells(grid : Array[Array[Int]], from : Int, to : Int): Unit = {

    for(row <- 1 until grid.length by 2) {

      for(column <- 1 until grid(0).length by 2) {

        if(grid(row)(column) == from) grid(row)(column) = to
      }
    }
  }

  /**
   * Check if a maze is fully generated.
   *
   * @param grid the grid of the maze.
   * @return true if the maze if fully generated or else false.
   */
  private def isGenerated(grid : Array[Array[Int]]) : Boolean = {

    for(row <- 1 until grid.length by 2) {

      for(column <- 1 until grid(0).length by 2) {

        if(grid(row)(column) != grid(1)(1)) return false
      }
    }
    true
  }

  /**
   * Generate entrance and exit coordinates of the maze.
   *
   * @param rows the number of rows of the maze.
   * @param columns the number of columns of the maze.
   * @return a couple of MazeCell object which corresponds to the entrance and the exit.
   */
  private def generateEntries(rows : Int, columns : Int) : (MazeCell, MazeCell) = {

    val randomNumber = RANDOM.nextInt(2)

    if(randomNumber == 0) {

      // Entry = Left, Exit = Right
      val entryRow : Int = RANDOM.between(1, rows - 1)
      val exitRow : Int = RANDOM.between(1, rows - 1)

      val entry : MazeCell = new MazeCell(if(entryRow % 2 == 0) entryRow - 1 else entryRow, 0)
      val exit : MazeCell = new MazeCell(if(exitRow % 2 == 0) exitRow - 1 else exitRow, columns - 1)

      (entry, exit)

    } else {

      // Entry = Top, Exit = Bottom
      val entryColumn : Int = RANDOM.between(1, columns - 1)
      val exitColumn : Int = RANDOM.between(1, columns - 1)

      val entry : MazeCell = new MazeCell(0, if(entryColumn % 2 == 0) entryColumn - 1 else entryColumn)
      val exit : MazeCell = new MazeCell(rows - 1, if(exitColumn % 2 == 0) exitColumn - 1 else exitColumn)

      (entry, exit)
    }
  }
}
